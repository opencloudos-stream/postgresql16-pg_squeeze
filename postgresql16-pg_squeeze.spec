%global sname pg_squeeze
%global pgsqueezerelversion %%(echo %{version} | tr '.' '_')
%ifarch loongarch64
%global llvm 0
%else
%global llvm 1
%endif
%global pgmajorversion 16

Summary:	A PostgreSQL extension for automatic bloat cleanup
Name:		postgresql%{pgmajorversion}-%{sname}
Version:	1.6.2
Release:	3%{?dist}
License:	PostgreSQL
Source0:	https://github.com/cybertec-postgresql/pg_squeeze/archive/REL%{pgsqueezerelversion}.tar.gz
URL:		https://github.com/cybertec-postgresql/%{sname}
BuildRequires:	postgresql%{pgmajorversion}-private-devel
# pg_config -> pg_server_config (postgresql-server-devel provides)
BuildRequires:	postgresql%{pgmajorversion}-server-devel
Requires:	postgresql%{pgmajorversion}-server

Obsoletes:	%{sname}%{pgmajorversion} < 1.3.0-2

%description
pg_squeeze is an extension that removes unused space from a table and
optionally sorts tuples according to particular index (as if CLUSTER
command was executed concurrently with regular reads / writes).

%if %llvm
%package llvmjit
Summary:	Just-in-time compilation support for pg_squeeze
Requires:	%{name} = %{version}-%{release}
Requires:	llvm

%description llvmjit
This packages provides JIT support for pg_squeeze
%endif

%prep
%autosetup -n %{sname}-REL%{pgsqueezerelversion}

%build
USE_PGXS=1 PG_CONFIG=%{_bindir}/pg_config %make_build

%install
USE_PGXS=1 %make_install

%{__mkdir} -p %{buildroot}%{_docdir}/pgsql/extension/
%{__cp} README.md %{buildroot}%{_docdir}/pgsql/extension/README-%{sname}.md

rm -f %{buildroot}/%{_docdir}/pgsql/extension/%{sname}.md

%files
%license LICENSE
%doc %{_docdir}/pgsql/extension/README-%{sname}.md
%{_libdir}/pgsql/%{sname}.so
%{_datadir}/pgsql/extension/%{sname}*.sql
%{_datadir}/pgsql/extension/%{sname}.control


%if %llvm
%files llvmjit
   %{_libdir}/pgsql/bitcode/%{sname}*.bc
   %{_libdir}/pgsql/bitcode/%{sname}/*.bc
%endif

%changelog
* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.6.2-3
- Rebuilt for loongarch release

* Wed Jun 12 2024 Zhai Liangliang <zhailiangliang@loongson.cn> - 1.6.2-2
- [Type] other
- [DESC] Fix build error for loongarch64

* Fri Apr 12 2024 Wang Guodong <gordonwwang@tencent.com> - 1.6.2-1
- init build
